<?php

/**
 * Implements COMMANDFILE_drush_command().
 */
function hosting_audit_drush_command() {
  $commands = array();
  $commands['hosting-audit-ipcheck'] = array(
    'aliases' => array(
      'hosting-ip',
      'ha-ip'
    ),
    'command-hook' => 'ipcheck',
    'description' => dt('Checks the ip of hosted sites and their aliases (optionally against saved values) to spot sites that have changed their DNS settinsg'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_DATABASE,
    'options' => array(
      'save_ips' => dt('Saves the IPs discovered during this run (default: FALSE)'),
      'save_destination' => dt('The file to save IPs to (default: ~/.drush/hosting_audit_ips)'),
      'load_ips' => dt('Loads a a file of domain name / ip pairs to check against (default: TRUE)'),
      'load_source' => dt('The file to load IPs to (default: ~/.drush/hosting_audit_ips)'),
      'quiet' => dt('Output less to stdout (default: FALSE)'),
      'default_ip' => dt('What the default IP should be if no saved ones are loaded or available'),
    ),
  );
  return $commands;
}

/**
 * Implements COMMANDFILE_init().
 */
function hosting_audit_drush_init() {
   if (!function_exists('provision_drush_init')) {
      drush_set_error('HOSTING_AUDIT_PROVISION_NOT_AVAILABLE',
                      dt('The provision drush command does not seem to be available.'));
   }
   _provision_drush_check_user();
}

/**
 * Implements drush_hook_COMMAND_validate().
 */
function drush_hosting_audit_ipcheck_validate() {
   $context = d();
   drush_log('Context type: ' . print_r($context->context_type, TRUE), 'debug');
   drush_log('Profile: ' . print_r($context->profile, TRUE), 'debug');
   if ($context->context_type != 'site' && $context->profile != 'hosting') {
      drush_set_error('HOSTING_AUDIT_NOT_HOSTMASTER',
                      dt('Site ' . $context->name . ' is not a hostmaster.'));
   }
}

/**
 * Callback for the hosting-audit-ipcheck drush command.
 */
function drush_hosting_audit_ipcheck() {
   // Set defaults.
   $quiet = FALSE;
   $destination = '~/.drush/hosting_audit_ips';
   $source = '~/.drush/hosting_audit_ips';
   $save = FALSE;
   $load = TRUE;
   $default_file = '/var/aegir/.drush/hosting_audit_ips.json';
   if ($t = drush_get_option('quiet', FALSE)) {
      $quiet = $t;
      drush_log('Set quiet to:'  . print_r($t, TRUE), 'debug');
   }
   if ($t = drush_get_option('save_destination', $default_file)) {
      $destination = $t;
      drush_log('Set destination to: ' . print_r($destination, TRUE), 'debug');
   }
   drush_log('destination: ' . print_r($destination, TRUE), 'debug');
   if ($t = drush_get_option('load_source', $default_file)) {
      $source = $t;
      drush_log('Set source to: ' . print_r($source, TRUE), 'debug');
   }
   if ($t = drush_get_option('save_ips', FALSE)) {
      $save = $t;
      drush_log('Set save to: ' . print_r($save, TRUE), 'debug');
   }
   if ($t = drush_get_option('load_ips', TRUE)) {
      $load = $t;
      drush_log('Set load to: ' . print_r($load, TRUE), 'debug');
   }
   $default = drush_get_option('default_ip', NULL);

   //$context = d();
   //drush_log(print_r($context, TRUE), 'debug');
   //drush_log(print_r($context->get_services(), TRUE), 'debug');
   //$db = $context->service('db');
   //drush_log(print_r($db, TRUE), 'debug');
   //$result = db_query('SELECT nid, type from {node} LIMIT 10');
   //foreach ($result as $row) {
   //   drush_log(print_r($row, TRUE), 'debug');
   //}

   // Load saved_ips if any & is enabled.
   // Errors here don't really impact the process much, so they are passed as warnings.
   $loaded = FALSE;
   if ($load && $source) {
      if (is_readable($source)) {
         if (!$data = file_get_contents($source)) {
            drush_log(dt('Failed to read content from source file:') . ' ' . $source,
                      'warning');
         }
         //echo print_r($data, TRUE);
         if(!$loaded = json_decode($data)) {
            drush_log(dt('Failed to decode content read from source file:') . ' ' . $source,
                      'warning');
         }
         //echo print_r($loaded, TRUE);
      }
   }
   else {
      drush_log(dt('Source file does not exist or is not readable: !source',
                   array('!source' => $source)), 'warning');
   }
   //drush_log('loaded: ' . print_r($loaded, TRUE), 'debug');

   // Seems like db_query runs against the bootstrapped it without any extra work.
   // Get all active sites.
   $result = db_query('SELECT n.nid as nid, n.vid as vid, n.title as title, group_concat(hsa.alias) as aliases from {node} as n left join ({hosting_site} as hs, {hosting_site_alias} as hsa) on (n.nid = hs.nid and n.vid = hs.vid and n.nid = hsa.nid and n.vid = hsa.vid) WHERE n.status = 1 and hs.status = 1 group by n.nid;');
   $sites = array();
   while ($row = db_fetch_array($result)) {
      //drush_log('row: ' . print_r($row, TRUE), 'debug');
      if (!isset($row['title'])) {
        continue;
      }
      $aliases = explode(',', $row['aliases']);
      if (!$aliases) {
         $aliases = array();
      }
      $sites[$row['title']] = array(
         'nid' => $row['nid'],
         'aliases' => array($row['title'] => array()),
      );
      foreach ($aliases as $alias) {
         $sites[$row['title']]['aliases'][$alias] = array();
      }
   }
   //drush_log(print_r($sites, TRUE), 'debug');

   // Ping each alias and record the result.
   $results = array();
   drush_log(dt('Found !x sites to check', array('!x' => sizeof($sites))), 'debug');
   foreach ($sites as $site_name => $site) {
      drush_log(dt('Checking aliases for site !site (node !nid)',
                   array('!site' => $site_name, '!nid' => $site['nid'])),
                'debug');
      foreach ($site['aliases'] as $alias => $data) {
         // Lookup. Only works for IPv4 :/.
         $ip = gethostbyname($alias);
         if ($ip != $alias) {
            drush_log(dt('!name resolved to !ip', array('!name' => $alias,
                                                        '!ip' => $ip)),
                      'debug');
            $sites[$site_name]['aliases'][$alias]['result'] = $ip;
         }
         else {
            drush_log(dt('Failed to resolve !site', array('!site' => $alias)),
                      'debug');
            $sites[$site_name]['aliases'][$alias]['result'] = 'ERROR';
         }

         // If we have saved information about the results, we can compare each alias to a previous run
         if ($loaded) {
            // @TODO Get saved run value and insert into saved_result.
            if (isset($loaded->$alias)) {
               $last_run = $loaded->$alias->result;
            }
         }
         if (!isset($last_run) && isset($default)) {
            $last_run = $default;
         }
         if (isset($last_run)) {
            if ($sites[$site_name]['aliases'][$alias]['result'] == $last_run) {
               $text = dt('No change');
            }
            else {
               $text = dt('WARNING! Changed');
            }
         }
         $results[$alias] = array(
            'domain name' => $alias,
            'nid' => $site['nid'],
            'result' => $sites[$site_name]['aliases'][$alias]['result'],
            'saved result' => isset($last_run) ? $last_run : '',
            'text' => isset($text) ? $text : '',
         );
      }
   }
   if (!$quiet) {
      drush_print_table($results);
   }
   //drush_log(print_r($sites, TRUE), 'debug');
   // Save, if enabled
   if ($save) {
      $json = json_encode($results);
      if (!$json) {
         drush_log(dt('Failed to encode results into json !err: !msg',
                      array('!err' => json_last_error(),
                            '!msg' => json_last_error_msg())),
                   'warning');
      }
      else {
         if ($size = file_put_contents($destination, $json) === FALSE) {
            drush_log(dt('Failed to write results to file !file',
                         array('!file' => $destination)), 'error');
         }
         else {
            drush_log(dt('Wrote !n bytes to !file',
                         array('!n' => $size, '!file' => $destination)),
                      'debug');
         }
      }
   }
}
